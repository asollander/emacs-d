
(message "Start of my emacs init file!")

; MATLAB Stuff
(add-to-list 'load-path "~/.emacs.d/matlab")
(require 'matlab-load)
(require 'tlc)
(add-to-list 'auto-mode-alist '("\\.tmf$" . makefile-mode))

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)
(unless (package-installed-p 'scala-mode2)
  (package-refresh-contents) (package-install 'scala-mode2))

;; Show column numbers
(setq column-number-mode t)

; Nice, unique file names
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
(message "End of my emacs init file")


